###############################################################
# initialisation
###############################################################

# clean up environment
rm(list = ls())

# set work directory
setwd('~/Documents/code/commercial_species')

# load data
plotDB <- readRDS('./data/plotDB.RDS')
treeDB <- readRDS('./data/treeDB.RDS')
comsp <- read.csv2('./data/essences_sodefor.csv')
dme <- read.csv2('./data/dme_sodefor.csv')
plant <- read.csv2('./data/planting_c4f_final.csv')

# Load packages
library(tidyverse)
library(ggplot2)
library(gridExtra)
library(BIOMASS)

###############################################################
# identify commercial species
###############################################################

# sp name correction
comsp <- comsp %>% mutate(sc_name = stringr::str_conv(sc_name, "UTF-8"))
comsp <- comsp %>% mutate(sc_name = case_when(sc_name == 'Lovoa trichilio�des' ~ 'Lovoa trichilioides',
                          sc_name == 'Copa�fera salikounda' ~ 'Copaifera salikounda',
                          sc_name == 'Pterocarpus santalino�des' ~ 'Pterocarpus santalinoides',
                          TRUE ~ sc_name),
                          backup = sc_name) %>%
                   separate(sc_name, c('genus', 'species'))

# correct names
comsp <- comsp %>% mutate(species = case_when(species == 'adolphi' & genus == 'Celtis' ~ 'adolfi-friderici',
                                              species == 'bongouanouensis' & genus == 'Ficus' ~ 'variifolia',
                                              TRUE ~ species))
taxo <- correctTaxo(genus = comsp$genus, species = comsp$species, useCache = FALSE)
comsp$genus <- taxo$genusCorrected
comsp$species <- taxo$speciesCorrected


###############################################################
# get commercial species dme
###############################################################

# sp name correction
dme <- dme %>% mutate(sc_name = stringr::str_conv(sc_name, "UTF-8"))
dme <- dme %>% mutate(sc_name = case_when(sc_name == 'Lovoa trichilio�des' ~ 'Lovoa trichilioides',
                          sc_name == 'Entandrophragma\ncylindricum' ~ 'Entandrophragma cylindricum',
                          sc_name == 'Combretodendron\nafricanum' ~ 'Combretodendron africanum',
                          sc_name == 'Entandrophragma\ncandollei' ~ 'Entandrophragma candollei',
                          sc_name == 'Amphimas pterocarpo�des ' ~ 'Amphimas pterocarpoides ',
                          sc_name == 'Distemonanthus\nbenthamianus' ~ 'Distemonanthus benthamianus',
                          TRUE ~ sc_name),
                          backup = sc_name) %>%
                   separate(sc_name, c('genus', 'species'))

# remove species with d_tol = NA
dme <- dme %>% filter(!is.na(d_tol))
# convert in cm
dme <- dme %>% mutate(d_tol = d_tol*100)

# correct names
dme <- dme %>% mutate(species = case_when(species == 'adolfi' & genus == 'Celtis' ~ 'adolfi-friderici',
                                              species == 'trichilio' & genus == 'Lovoa' ~ 'trichilioides',
                                              TRUE ~ species))
taxo <- correctTaxo(genus = dme$genus, species = dme$species, useCache = FALSE)
dme$genus <- taxo$genusCorrected
dme$species <- taxo$speciesCorrected

###############################################################
# create commercial species database
###############################################################

# commercial species database
comDB <- inner_join(comsp %>% select(genus, species), treeDB, by = c('genus', 'species'))

# TODO: vérifier dans les sp exclues qu'il n'y a pas d'sp commerciales

# remove trees with dbh = hlog = origin = NA (tree loss: 87 trees = 3.46%)
comDB <- comDB %>% filter(!is.na(dbh), !is.na(hlog), !is.na(origin))
comDB <- comDB %>% filter(dbh>=10) %>% ungroup()
comDB <- as_tibble(comDB)

# n.ha-1
df <- comDB %>% group_by(clus, type) %>%
                summarise(n = n()) %>%
                ungroup() %>%
                complete(clus, type) %>%
                replace(is.na(.), 0) %>%
                left_join(plotDB, by = c('clus', 'type')) %>%
                mutate(n_ha = n/area)
quantile(df$n_ha)

# BA.ha-1
df <- comDB %>% mutate(ba = pi*((dbh/100)/2)^2)%>%
                group_by(clus, type) %>%
                summarise(BA = sum(ba)) %>%
                ungroup() %>%
                complete(clus, type) %>%
                replace(is.na(.), 0) %>%
                left_join(plotDB, by = c('clus', 'type')) %>%
                mutate(BA_ha = BA/area)
quantile(df$BA_ha)

###############################################################
# tree origin in each clusters
###############################################################

# dataframe
df <- comDB %>% group_by(clus, type, origin) %>% summarise(n = n()) %>%
                ungroup() %>%
                complete(clus, type, origin) %>%
                replace(is.na(.), 0) %>%
                group_by(clus) %>% mutate(prop = n * 100 / sum(n))

# propotion of planted trees (all plots)
prop1 <- df %>% filter(origin == 'planted')
sum(prop1$n) *100 / sum(df$n)

# propotion of planted trees (clusters with plantation programs)
clusdf <- df %>% filter(!(clus %in% c('bian', 'bono', 'guey')))
treedf <- clusdf %>% filter(origin == 'planted')
sum(treedf$n) * 100 / sum(clusdf$n)

# propotion of planted trees (in plots where trees were planted)
plan <- plant %>% select(plot_ID, planting) %>%
                  mutate(clus = str_split_fixed(plant$plot_ID, '_', 2)[1:150],
                         type = str_split_fixed(plant$plot_ID, '_', 2)[151:300],
                         clus = substr(clus,1, 4))
plots <- df %>% left_join(plan, by = c('clus', 'type')) %>%
                filter(planting == 1)
ntot <- sum(plots$n)
plots <- plots %>% filter(origin == 'planted')
npla <- sum(plots$n)
npla * 100 / ntot

# sort clusters depending on number of planted trees
df <- df %>% group_by(clus, origin) %>% summarise(n = sum(n), prop = sum(prop))
tab <- df %>% filter(origin == 'planted') %>% select(clus, prop)
df <- left_join(df, tab, by = 'clus', suffix = c('', 'plant')) %>% mutate(propplant = replace_na(propplant,0))
# df$propplant = df$propplant %>% replace_na(0)
df <- df %>% arrange(-propplant)
df$clus <- factor(df$clus, levels = unique(df$clus))

# change 'planted for '(trans)planted'
df <- df %>% mutate(origin = as.character(origin),
                    origin = case_when(origin == 'planted' ~ '(trans)planted',
                              TRUE ~ origin))
# reorder origin factor
df$origin <- factor(df$origin, levels = c('spontaneous', 'remnant', '(trans)planted'))

# plot
pl0 <- ggplot(data = df) +
geom_bar(aes(x = clus, y = prop, fill = origin), stat = 'identity') +
ylab('proportion (%)') +
theme_bw() +
theme(legend.position = "bottom",
      axis.title.x = element_blank(),
      legend.title = element_blank()) +
scale_y_continuous(limits = c(0,101), expand = c(0, 0)) +
# scale_fill_brewer(palette = "Greys")
scale_fill_manual("legend", values = c("spontaneous" = "green3", "remnant" = "orange4", "(trans)planted" = "red"))
ggsave(file = 'plant.jpg', plot = pl0, width = 7, height = 5)

###############################################################
# assess timber resource
###############################################################

classify_resource <- function(treeData, dmeData){
  # add dme
  df <- left_join(treeData, dmeData %>% select(genus, species, d_tol), by = c('genus', 'species'))
  # if minimum diameter = NA -> assign min(dme)
  df <- df %>% mutate(d_tol = ifelse(is.na(d_tol), min(df$d_tol, na.rm = T), d_tol))
  # classify trees depending on whether their dbh is > or < d_tol
  df <- df %>% mutate(harvestable = case_when(dbh >= d_tol ~ 1,
                                              dbh < d_tol ~ 0))
  # classify trees depending on their conformation
  df <- df %>% mutate(conf = case_when(str != 2 & cyl != 2 & health == 1 ~ 'good',
                                       str == 2 | cyl == 2 | health == 2 ~ 'poor',
                                       is.na(str) | is.na(cyl) | is.na(health) ~ 'poor'))
  # define current and future resource
  df <- df %>% mutate(resource = case_when(harvestable == 1 & hlog >= 5 & conf == 'good' ~ 'current',
                                           harvestable == 0 & conf == 'good' ~ 'future',
                                           TRUE ~ 'poor'))
  return(df)
}

resource <- function(treeData, dmeData, plotData){
  df <- classify_resource(treeData, dmeData)
  # select resource
  # df <- df %>% filter(resource == res)
  # volume, basal area and density
  df <- df %>% mutate(vol = (dbh/100)*hlog*0.4, ba = pi*((dbh/100)/2)^2) %>% group_by(clus, type, origin, resource) %>% summarise(vol = sum(vol), n = n(), BA = sum(ba))
  # Generate all possible combinations of `type`, `origin`
  # to account for the absence of some origins in some plots
  df <- as_tibble(df) %>% complete(clus, type, origin, resource)
  # get plot area
  df <- left_join(df, plotData, by = c('clus', 'type')) %>% mutate(vol_ha = vol / area, n_ha = n / area, BA_ha = BA / area)
  # format wide to long
  df <- df %>% select(- area) %>% pivot_longer(cols = c(vol_ha, n_ha, BA_ha, vol, BA, n), names_to = 'var', values_to = 'value') %>%
               mutate(value = replace_na(value,0))
  # reorder origin factor
  df$origin <- factor(df$origin, levels = c('remnant', 'spontaneous', 'planted'))
  return(df)
}

plotResource <- function(data){
  # plot
  df <- data %>% mutate(var = case_when(var == 'vol_ha' ~ 'volume',
                                          var == 'n_ha' ~ 'density',
                                          var == 'BA_ha' ~ 'basal area',
                                          TRUE ~ var))
  # change 'planted for '(trans)planted'
  df <- df %>% mutate(origin = as.character(origin),
                      origin = case_when(origin == 'planted' ~ '(trans)planted',
                                TRUE ~ origin))
  # reorder origin factor
  df$origin <- factor(df$origin, levels = c('remnant', 'spontaneous', '(trans)planted'))
  # plot
  pl <- ggplot(data = df %>% filter(resource != 'poor', !(var %in% c('vol', 'BA', 'n')))) +
  geom_boxplot(aes(x = origin, y = value, col = origin)) +
  facet_grid(var ~ resource, scales = 'free') +
  # scale_y_continuous(trans='log10') +
  # scale_y_sqrt() +
  # scale_y_log10() +
  coord_trans(y = "log1p") +
  scale_color_manual("legend", values = c("spontaneous" = "green3", "remnant" = "orange4", "(trans)planted" = "red")) +
  theme_bw() +
  theme(axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        strip.background = element_blank(),
        legend.position = "bottom",
        legend.title = element_blank())
  return(pl)
}

synth <- function(data){
  syn <- data %>% filter(!(var %in% c('vol', 'BA', 'n'))) %>%
                  group_by(origin, var) %>%
                  summarise(Q00 = quantile(value, probs = 0),
                            Q25 = quantile(value, probs = 0.25),
                            median = quantile(value, probs = 0.5),
                            Q75 = quantile(value, probs = 0.75),
                            Q100 = quantile(value, probs = 1)) %>%
                  mutate(across(c(Q00, Q25, median, Q75, Q100), num, digits = 1))
}

# overview
overview <- function(df, remnant){
  if(remnant == 0){
    df <- df %>% mutate(concat = paste0(resource, origin)) %>%
                  filter(concat != 'futureremnant')
  } else if(remnant == 1){
    df <- df
  }
  tot <- df %>% group_by(resource, var) %>%
                summarise(value = sum(value)) %>%
                filter(resource != 'poor') %>%
                rename(tot = value)
  ov <- df %>% group_by(origin, resource, var) %>%
               summarise(value = sum(value)) %>%
               filter(resource != 'poor', !(var %in% c('vol_ha', 'BA_ha', 'n_ha'))) %>%
               left_join(tot, by = c('resource', 'var')) %>%
               mutate(prop = value * 100 / tot) %>%
               arrange(resource, var, origin)

  # change 'planted for '(trans)planted'
  ov <- ov %>% mutate(origin = as.character(origin),
                      origin = case_when(origin == 'planted' ~ '(trans)planted',
                                TRUE ~ origin),
                      var = case_when(var == 'vol' ~ 'V',
                                      TRUE ~ var))
  # reorder origin factor
  ov$origin <- factor(ov$origin, levels = c('(trans)planted', 'spontaneous', 'remnant'))

  # plot overview
  pl1 <- ggplot(data = ov) +
  geom_bar(aes(x = var, y = prop, fill = origin), stat = 'identity') +
  facet_wrap(~ resource) +
  theme_bw() +
  ylab('proportion (%)') +
  theme(legend.position = "bottom",
        axis.title.x = element_blank(),
        legend.title = element_blank(),
        strip.background = element_blank()) +
  scale_y_continuous(limits = c(0,101), expand = c(0, 0)) +
  # scale_fill_brewer(palette = "Greys")
  scale_fill_manual("legend", values = c("spontaneous" = "green3", "remnant" = "orange4", "(trans)planted" = "red"))
  if(remnant == 0){
    ggsave(file = 'resource_without_remnant.jpg', plot = pl1, width = 5, height = 7)
  } else if(remnant == 1){
    ggsave(file = 'resource.jpg', plot = pl1, width = 5, height = 7)
  }
  return(ov)
}

# calculate resource
df <- resource(treeData = comDB, plotData = plotDB, dmeData = dme)

# overview
ov <- overview(df, remnant = 0) %>% mutate(across(c(value, tot, prop), num, digits = 1))
ov <- overview(df, remnant = 1) %>% mutate(across(c(value, tot, prop), num, digits = 1))

# plot resource.ha-1
pl2 <- plotResource(data = df)
ggsave(file = 'resource_ha.jpg', plot = pl2, width = 7, height = 7)

# plot resource.ha-1 without future remnant
df2 <- df %>% mutate(concat = paste0(resource, origin)) %>%
              filter(concat != 'futureremnant')
pl3 <- plotResource(data = df2)
ggsave(file = 'resource_ha_without_remnant.jpg', plot = pl3, width = 7, height = 7)

# current resource: extract numbers
cur <- df %>% filter(resource == 'current')
syn <- synth(cur)

# future resource: extract numbers
fut <- df %>% filter(resource == 'future')
syn <- synth(fut)

###############################################################
# list
###############################################################

# get tree conformation
df <- classify_resource(treeData = comDB, dmeData = dme)

# remove remnant trees
df <- df %>% filter(origin != 'remnant')

# count number of good and poor trees for each sp
nbGoodPoor <- df %>% group_by(genus, species) %>% count(conf) %>%
              pivot_wider(names_from = conf, values_from = n) %>%
              replace(is.na(.), 0) %>%
              mutate(conf = good>poor,
                     nb = good + poor)

# proportion of planted / spontaneous for each species
propPlanSpon <- df %>% group_by(genus, species) %>% count(origin) %>%
                pivot_wider(names_from = origin, values_from = n) %>%
                replace(is.na(.), 0) %>%
                mutate(tot = spontaneous + planted,
                       propSpon = spontaneous * 100 / tot,
                       propPlan = planted * 100 / tot)

# merge tables and remove sp with less than 10 trees
df <- full_join(nbGoodPoor, propPlanSpon, by = c('genus', 'species')) %>%
      filter(nb >= 10)

# list of species with more good trees than poor trees
# and more than 90% spontaneous trees
spon <- df %>% filter(conf == TRUE, propSpon >= 90) %>%
               mutate(species = paste(genus, species))
sort(spon$species)

# list of species with more good trees than poor trees
# and more than 90% planted trees
plan <- df %>% filter(conf == TRUE, propPlan >= 90) %>%
               mutate(species = paste(genus, species))
sort(plan$species)

# list of species with more good trees than poor trees
# and more than 10% planted trees and 10% spontaneous trees
both <- df %>% filter(conf == TRUE, propSpon >= 10, propPlan >= 10) %>%
               mutate(species = paste(genus, species))
sort(both$species)

# list of species with more good trees than poor trees
poorsp <- df %>% filter(conf == FALSE) %>%
               mutate(species = paste(genus, species))
sort(poorsp$species)

###############################################################
# conformation
###############################################################

# get resource class
df <- classify_resource(treeData = comDB, dmeData = dme)

# # remove 600 trees with pru = NA (-3.22 with no dbh... - 24.72. total loss = 27.94%)
# df <- df %>% filter(!is.na(pru))

# count tree pruned/unpruned with good or poor confirmation
df <- df %>% mutate(pru = as.character(pru)) %>%
             mutate(pru = case_when(pru == '0' ~ 'pruned',
                                    TRUE ~ 'unpruned')) %>%
             group_by(origin, conf, pru) %>% summarise(n = n()) %>%
             group_by(origin) %>%
             mutate(N = sum(n), prop = n/N*100)

# plot
# poor trees are not distinguished by 'pruned' / 'unpruned' in the plot
dfgood <- df %>% filter(conf == 'good') %>% select(origin, prop, pru)
dfpoor <- df %>% filter(conf == 'poor') %>% summarise(prop = sum(prop)) %>% mutate(pru = 'poor')
df <- bind_rows(dfgood, dfpoor) %>% mutate(across(prop, num, digits = 1))

# change 'planted for '(trans)planted' and add conformation to pru
df <- df %>% mutate(origin = as.character(origin),
                    origin = case_when(origin == 'planted' ~ '(trans)planted',
                              TRUE ~ origin),
                    pru = as.character(pru),
                    pru = case_when(pru == 'pruned' ~ 'well-conformed - pruned',
                                    pru == 'unpruned' ~ 'well-conformed - unpruned',
                                    pru == 'poor' ~ 'poorly conformed',
                                    TRUE ~ pru))
# reorder origin and pru factors
df$origin <- factor(df$origin, levels = c('spontaneous', 'remnant', '(trans)planted'))
df$pru <- factor(df$pru, levels = c('poorly conformed', 'well-conformed - unpruned', 'well-conformed - pruned'))

# manage colors
df <- df %>% mutate(color = paste0(origin, pru))

#
pl4 <- ggplot(data = df) +
geom_bar(aes(x = origin, y = prop, fill = color), stat = 'identity') +
ylab('proportion (%)')+
theme_bw() +
theme(legend.position = "bottom",
      axis.title.x = element_blank(),
      legend.title = element_blank()) +
scale_y_continuous(limits = c(0,101), expand = c(0, 0)) +
# scale_fill_brewer(palette = "Greys") +
scale_fill_manual("legend", values = c("(trans)plantedwell-conformed - pruned" = "red3",
                                       "(trans)plantedwell-conformed - unpruned" = 'red4',
                                       "(trans)plantedpoorly conformed" = 'red2',
                                       "remnantwell-conformed - pruned" = 'orange3',
                                       "remnantwell-conformed - unpruned" = 'orange4',
                                       "remnantpoorly conformed" = 'orange2',
                                       "spontaneouswell-conformed - pruned" = 'green2',
                                       "spontaneouswell-conformed - unpruned" = 'green3',
                                       "spontaneouspoorly conformed" = 'green1'))
ggsave(file = 'pruning.jpg', plot = pl4, width = 5, height = 7)
